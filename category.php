<?php

include __DIR__ . '/vendor/autoload.php';

use Rubix\ML\Datasets\Labeled;
use Rubix\ML\Datasets\Unlabeled;
use Rubix\ML\Extractors\JSON;
use Rubix\ML\Classifiers\KNearestNeighbors;
use Rubix\ML\CrossValidation\Metrics\Accuracy;
use Rubix\ML\Other\Tokenizers\NGram;
use Rubix\ML\PersistentModel;
use Rubix\ML\Persisters\Filesystem;
use Rubix\ML\Transformers\TextNormalizer;
use Rubix\ML\Transformers\WordCountVectorizer;
use Rubix\ML\Transformers\TfIdfTransformer;

echo 'Loading data into memory ...' . PHP_EOL;

//$sample = include 'string.php';
//$label = ['laptop','laptop','laptop','laptop','phone','phone','phone','phone'];
//$training = Labeled::fromIterator(new CSV('dataset.csv'));

//$training = Labeled::fromIterator(new JSON('data.json'));
//$training = new Labeled($sample,$label);
//$extractor = new ColumnPicker(new JSON('datatext.json'),['title','label']);
//$extractor2 = new ColumnPicker(new JSON('data.json'),['description']);
$training = Labeled::fromIterator(new JSON('learn-text.json'))
    ->apply(new TextNormalizer())
    ->apply(new WordCountVectorizer(5000))
    ->apply(new TfIdfTransformer());
echo $training;

$dataset = Unlabeled::fromIterator(new JSON('dataset-text.json'))
    ->apply(new TextNormalizer())
    ->apply(new WordCountVectorizer(5000))
    ->apply(new TfIdfTransformer());
echo $dataset;
$estimator = new KNearestNeighbors(5);

echo 'Training ...' . PHP_EOL;

$estimator->train($training);

echo 'Making predictions ...' . PHP_EOL;

$predictions = $estimator->predict($dataset);

echo 'Example predictions:' . PHP_EOL;

print_r($predictions);

$metric = new Accuracy();

$score = $metric->score($predictions, $dataset->labels());

echo 'Accuracy is ' . (string) ($score * 100.0) . '%' . PHP_EOL;
