<?php
include __DIR__ . '/vendor/autoload.php';

use Rubix\ML\Classifiers\MultilayerPerceptron;
use Rubix\ML\Classifiers\SoftmaxClassifier;
use Rubix\ML\Datasets\Labeled;
use Rubix\ML\Extractors\JSON;
use Rubix\ML\Other\Tokenizers\NGram;
use Rubix\ML\PersistentModel;
use Rubix\ML\Persisters\Filesystem;
use Rubix\ML\Pipeline;
use Rubix\ML\Transformers\TextNormalizer;
use Rubix\ML\Transformers\TfIdfTransformer;
use Rubix\ML\Transformers\WordCountVectorizer;
use Rubix\ML\Transformers\ZScaleStandardizer;

$training = Labeled::fromIterator(new JSON('datatext.json'));
$estimator = new PersistentModel(

    new Pipeline([
        new TextNormalizer(),
        new WordCountVectorizer(10000, 2, 10000, new NGram(1, 2)),
        new TfIdfTransformer(),
//        new ZScaleStandardizer(),
    ], new SoftmaxClassifier(128)),
    new Filesystem('data.model', true)
);
$estimator->train($training);
$estimator->save();
