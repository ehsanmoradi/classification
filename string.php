<?php
$string = [
    ['8 گیگابایت', 'یک ترابایت', 'NVIDIA', 15.6, 'Core i5', 'فاقد ویندوز'],
    ['12 گیگابایت', 'دو ترابایت', 'NVIDIA', 17, 'Core i3', 'ویندوز 7'],
    ['16 گیگابایت', 'دو ترابایت', 'NVIDIA', 15.6, 'Core i7', 'ویندوز 10'],
    ['16 گیگابایت', 'یک ترابایت', 'NVIDIA', 14.6, 'Core i7', 'ویندوز 10'],
    ['2 گیگابایت', '64 گیگابایت', 'Mali G72', 6, 'Exynos 9611', 'اندروید'],
    ['2 گیگابایت', '128 گیگابایت', 'Mali G22', 5.8, 'Exynos 9611', 'آی او اس'],
    ['4 گیگابایت', '32 گیگابایت', 'Mali G22', 5.5, 'Quad core Cortex A73', 'اندروید'],
    ['4 گیگابایت', '512 گیگابایت', 'Mali G22', 6.5, 'Quad core Cortex A53', 'اندروید'],
];
$int = [];
$csv = fopen('dataset.csv','w');

foreach ($string as $index => $items) {
    foreach ($items as $item) {
        $int[$index][] = crc32($item);
    }
    fputcsv($csv,$int[$index]);
}

fclose($csv);
return $int;
//print_r($int);
